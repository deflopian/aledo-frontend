"use strict";

const gulp = require('gulp'),
  rimraf = require('gulp-rimraf'),
  pug = require('gulp-pug'),
  scss = require('gulp-sass'),
  posthtml = require('gulp-posthtml'),
  concat = require('gulp-concat'),
  browserSync = require('browser-sync'),
  postcss = require('gulp-postcss'),
  usedcss = require('usedcss'),
  autoprefixer = require('autoprefixer'),
  nano = require('cssnano'),
  uglify = require('gulp-uglifyjs'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  mozjpeg = require('imagemin-mozjpeg'),
  typograf = require('gulp-typograf');

const multipage = true;

const pages = [
  //'layout',
  'layout_reduced',
  /* 'index',
  'projects',
  'project',
  'services',
  'brands',
  'solutions',
  'solutions-cinema',
  'solutions-lines',
  'serie', */
  'serie-configurator',
  //'catalog',
];

const paths = {
  root: 'dest',
  scss: 'src/scss/',
  pug: 'src/pages/',
  css: 'dest/css',
  img: 'src/img',
  js: 'src/*blocks/',
  build: 'build'
};

gulp.task('browser-sync', ['scss', 'pug', 'copy'], () => {
  browserSync({
    server: "./dest",
    startPath: "index.html",
    open: false,
    notify: false,
    browser: false
  });
});

gulp.task('pug', () => {
  var plugins = [
      require('posthtml-bem')({
      elemPrefix: '__',
      modPrefix: '_',
      modDlmtr: '-'
    })];
  pages.forEach(function (item, i, arr) {
    while (i < pages.length) {
      return gulp.src([paths.pug + item + '.pug'])
        .pipe(pug({
          pretty: true
        }))
        .pipe(posthtml(plugins))     
        .pipe(typograf({ 
          locale: ['ru', 'en-US'],
          htmlEntity: { type: 'name' },
          enableRule: ['ru/money/ruble']
        }))
        .pipe(gulp.dest(paths.root));
    }
  });
});

gulp.task('scss', () => {
  pages.forEach(function (item, i, arr) {
    while (i < pages.length) {
      return gulp.src(paths.scss + item + '.scss')
        .pipe(scss({
          includePaths: [paths.scss]
          /*,
                outputStyle: 'compressed'*/
        }))
        .on('error', scss.logError)
        .pipe(gulp.dest(paths.css))
        .pipe(browserSync.reload({
          stream: true
        }));
    }
  });
});

gulp.task('pug-rebuild', ['pug'], () => {
  browserSync.reload();
});

gulp.task('browser-reload', () => {
  browserSync.reload();
});

gulp.task('watch', () => {
  gulp.watch('**/*.scss', ['scss']);
  gulp.watch('**/*.pug', ['pug-rebuild']);
  gulp.watch(paths.img + '/*.{png,jpg,svg}', ['copy-img', 'browser-reload']);
  gulp.watch(paths.js + '**/*.js', ['copy-js', 'browser-reload']);
});

gulp.task('default', ['browser-sync', 'watch']);


// Clear dest
gulp.task('clear', () => {
  return gulp.src('./dest/**, ./build/**', {
      read: false
    })
    .pipe(rimraf());
});

// Copy js and img to dest
gulp.task('copy', ['libs-js', 'copy-js', 'copy-img']);

gulp.task('copy-js', () => {
  pages.forEach(function (item, i, arr) {
    while (i < pages.length) {
      return gulp.src("src/" + item + "-blocks/**/*.js")
        .pipe(concat(item + '-main.js'))
        .pipe(gulp.dest("dest/js"))
    }
  });
});

gulp.task('copy-img', () => {
  return gulp.src("src/img/*.{png,jpg,svg}")
    .pipe(gulp.dest("dest/img"));
});

gulp.task('libs-js', () => {
  return gulp.src("src/libs/js/*.js")
    .pipe(concat('libs.js'))
    .pipe(gulp.dest("dest/js"))
});

// BUILD 
gulp.task('build-css', () => {
  var usedOptions = {
    html: [paths.build + `/*.html`],
    js: [paths.build + `js/*.js`],
    ignoreNesting: true,
    //ignoreRegexp: [/flickity*/, /fancybox*/, /collaps*/, /modal*/, /show/, /popper*/, /popover/, /_/, /x\-placement*/],
  };
  var nanoOptions = {
    core: false,
    discardComments: false
  };
  pages.forEach(function (item, i, arr) {
    while (i < pages.length) {
      return gulp.src(paths.scss + item + '.scss')
        .pipe(scss({
          includePaths: [paths.scss]
        }))
        .on('error', scss.logError)
        .pipe(postcss([
          usedcss(usedOptions),
          autoprefixer,
          nano(nanoOptions)
        ]))
        .pipe(gulp.dest(paths.build + '/css'))    
    }
  });
});
//
gulp.task('build-html', () => {
  var plugins = [
      require('posthtml-bem')({
      elemPrefix: '__',
      modPrefix: '_',
      modDlmtr: '-'
    })];
  pages.forEach(function (item, i, arr) {
    while (i < pages.length) {
      return gulp.src([paths.pug + item + '.pug'])
        .pipe(pug({
          pretty: true,
          data: {
            title: item
          }
        }))
        .pipe(posthtml(plugins))
        .pipe(typograf({ 
          locale: ['ru', 'en-US'],
          htmlEntity: { type: 'name' },
          enableRule: ['ru/money/ruble'],
        }))
        .pipe(gulp.dest(paths.build));
    }
  });
});
//
gulp.task('build-js', () => {
  pages.forEach(function (item, i, arr) {
    while (i < pages.length) {
      return gulp.src("src/" + item + "-blocks/**/*.js")
        .pipe(concat(item + '-main.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(paths.build + "/js"))
    }
  });
});

gulp.task('build-img', () => {
  return gulp.src("src/img/*.{png,jpg,svg}")
    /*.pipe(imagemin([
            pngquant(),
            mozjpeg({
        progressive: true
      })
        ], {
      verbose: true
    }))*/
    .pipe(gulp.dest(paths.build + "/img"))
});

gulp.task('build-libs', () => {
  return gulp.src("src/libs/js/*.js")
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.build + "/js"))
});

//
gulp.task('build', [
  'build-css', 
  'build-html', 
  'build-js', 
  'build-img', 
  'build-libs'
]);

gulp.task('build-test', () => {
  browserSync({
    server: "./build",
    startPath: "index.html",
    open: false,
    notify: false,
    browser: false
  });
});
