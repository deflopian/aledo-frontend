//
// POPOVERS
//
$('.aledo__popover-dismiss').popover({
  trigger: 'focus',
  html: true,
  template: '<div class="aledo__popover popover" role="tooltip">' +
    '<div class="aledo__popover-content popover-content"></div></div>'
});