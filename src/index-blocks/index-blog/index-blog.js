/* INDEX BLOG */

// SLIDER

$('.index-blog__blog-slider').flickity({
  // options  
  pageDots: true,
  contain: true,
  //setGallerySize: false
});

$(document).ready(function () {
  setTimeout(function () {
    $('.index-blog__blog-slider').css({opacity: 1, height: 'auto'});
  }, 100);
});



// ELLIPSIS
function blogShort() {
  $(function () {
    if ($(window).width() > 1199) {
      $('.index-blog__post-desc').succinct({
        size: 300
      });
    } else if ($(window).width() > 991) {
      $('.index-blog__post-desc').succinct({
        size: 240
      });
    } else {
      $('.index-blog__post-title').succinct({
        size: 48
      });
      $('.index-blog__post-desc').succinct({
        size: 120
      });
    }
  });
}
$(document).ready(blogShort);
$(window).resize(blogShort);
