/* inPROJECTS */

// ELLIPSIS
function inProjectShort() {
  $(function () {
    if ($(window).width() > 991) {
      $('.index-projects__project-desc').succinct({
        size: 360
      });
    } else {
      $('.index-projects__project-desc').succinct({
        size: 240
      });
    }
  });
}
$(document).ready(inProjectShort);
$(window).resize(inProjectShort);

//
// NAV SLIDER
var $carousel = $('.index-projects__projects-nav').flickity({
  // options  
  pageDots: false,
  wrapAround: true,
  contain: true,
  setGallerySize: false,
  dragThreshold: 10,
  sync: '.index-projects__projects-wrap'
});

$carousel.on('staticClick.flickity', function (event, pointer, cellElement, cellIndex) {
  $carousel.flickity('select', cellIndex);
});

// PROJECT SLIDER
$('.index-projects__projects-wrap').flickity({
  // options  
  pageDots: false,
  prevNextButtons: false,
  wrapAround: true,
  draggable: false,
  setGallerySize: false
});

// DETAILS SLIDER
$('.index-projects__project-slider').flickity({
  // options  
  pageDots: false,
  wrapAround: true,
  dragThreshold: 10,
  setGallerySize: false  
});

/* LARGE TEXT LINE */

$(document).ready(function () {
  $('.thumbnails__name').mouseover(function () {
    var nameWidth = $(this).width();
    var text = $(this).children();
    var clone = text.clone(true, true);
    clone.css({
      position: 'absolute',
      visibility: 'hidden',
      textTransform: 'uppercase',
      fontSize: '1.4rem',
      top: -1000,
      left: -1000
    })
    if ( $(this).parent().hasClass( "is-selected" ) ) {
      clone.css({
        fontWeight: 500
      })
    } else {
      clone.css({
        fontWeight: 300
      })
    }      
    clone.appendTo("body");
    var textWidth = clone.width();
    clone.remove();
    if (nameWidth < textWidth) {
      var df = textWidth - nameWidth;
      var dur = df / 150;
      text.css({
        marginLeft: -df,
        transition: 'margin-left ' + dur + 's linear .2s'
      });
    }
  });
  $('.thumbnails__name').mouseout(function () {
    var text = $(this).children();
    text.css({
      marginLeft: 0
    });
  });
});