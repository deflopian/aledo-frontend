/* LARGE TEXT LINE */

$(document).ready(function () {
  $('.thumbnails__name').mouseover(function () {
    var nameWidth = $(this).width();
    var text = $(this).children();
    var clone = text.clone(true, true)
      .css({
        position: 'absolute',
        visibility: 'hidden',
        textTransform: 'uppercase',
        fontWeight: 300,
        fontSize: '1.4rem',
        top: -1000,
        left: -1000
      })
      .appendTo("body");
    var textWidth = clone.width();
    clone.remove();
    if (nameWidth < textWidth) {
      var df = textWidth - nameWidth;
      var dur = df / 150;
      text.css({
        marginLeft: -df,
        transition: 'margin-left ' + dur + 's linear .2s'
      });
    }
  });
  $('.thumbnails__name').mouseout(function () {
    var text = $(this).children();
    text.css({
      marginLeft: 0
    });
  });
});

/* PANE SLIDER */

$('.tabs__pane-slider').flickity({
  // options  
  wrapAround: true,
  contain: true,
  pageDots: false,
  draggable: false
});

$('.tabs__pane-scroller').flickity({
  // options  
  cellAlign: 'left',
  freeScroll: true,
  //freeScrollFriction: 0.05,
  contain: true,
  prevNextButtons: false,
  pageDots: false
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
  $('.tabs__pane-slider').flickity('resize');
  $('.tabs__pane-scroller').flickity('resize');
});

/* Fancybox */
$("[data-fancybox]").fancybox({
  buttons: [
    'close'
  ],
  clickContent: function (current, event) {
    return current.type === 'image' ? 'close' : false;
  },
  lang: 'ru',
  i18n: {
    'en': {
      CLOSE: 'Close',
      NEXT: 'Next',
      PREV: 'Previous',
      ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
      PLAY_START: 'Start slideshow',
      PLAY_STOP: 'Pause slideshow',
      FULL_SCREEN: 'Full screen',
      THUMBS: 'Thumbnails'
    },
    'ru': {
      CLOSE: 'Закрыть',
      NEXT: 'Следующий',
      PREV: 'Предыдущий',
      ERROR: 'Ошибка: перезаргузите страницу.',
      PLAY_START: 'Старт',
      PLAY_STOP: 'Стоп',
      FULL_SCREEN: 'Полный экран',
      THUMBS: 'Меню'
    }
  }
});

/* MODAL */

$('.solutions-cinema-sample-modal__project-slider_dial').flickity({
  // options  
  pageDots: false,
  wrapAround: true,
  sync: '.solutions-cinema-sample-modal__project-slider_photos'
});

$('.solutions-cinema-sample-modal__project-slider_photos').flickity({
  // options  
  pageDots: false,
  wrapAround: true
});

$('#js-solutions-cinema-sample-modal').on('shown.bs.modal', function (event) {
  $('.solutions-cinema-sample-modal__project-slider_dial').flickity('resize');
  $('.solutions-cinema-sample-modal__project-slider_photos').flickity('resize');
});