/* inSHOWCASE */

// ELLIPSIS

function showcaseShort() {
  $(function () {
    if ($(window).width() > 991) {
      $('.index-showcase__desc').succinct({
        size: 200
      });
    } else {
      $('.index-showcase__desc').succinct({
        size: 100
      });
    }
  });
}
$(document).ready(showcaseShort);
$(window).resize(showcaseShort);
