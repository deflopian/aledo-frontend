/* index-vendors */

$('.index-vendors__slider').flickity({
  // optilons  
  pageDots: false,
  prevNextButtons: false,
  wrapAround: true,
  freeScroll: true,
  dragThreshold: 10,
  freeScrollFriction: 0.05
});

$(document).ready(function () {
  var $img = $('.index-vendors__slide');
  var numItems = $img.length;
  for (var i = 0; i < numItems; i++) {
    that = $img[i];
    var bg = $(that).css('background-image');
    bg = bg.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    img = new Image();
    img.src = bg;
    var imgWidth = img.width * .75;
    $(that).css({
      'width': imgWidth
    });    
  }
  setTimeout(function () {
    $('.index-vendors__slider').flickity('resize');
    $('.index-vendors__slide').css('opacity', .7);
  }, 100);
});
