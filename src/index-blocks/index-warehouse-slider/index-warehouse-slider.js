// SLIDER

$('#js-index-warehouse-slider').flickity({
  // options  
  wrapAround: true,
  groupCells: true,
  groupCells: 1,
  freeScroll: false,
  contain: false,
  pageDots: false,
  dragThreshold: 10,
  selectedAttraction: 0.05,
  friction: 0.5
});

$(document).ready(function () {
  setTimeout(function () {
    $('.index-warehouse-slider__card').css('opacity', 1);
  }, 100);
});
