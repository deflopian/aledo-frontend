// HEADER
//
// SCROLL TO STICK
$(window).on('scroll', function (event) {
  var scrollValue = $(window).scrollTop();
  var headerHeight = $('.header').height();
  if (scrollValue > 40) {
    $('.js-header').addClass('header_stuck');
    $('.body').css('margin-top', headerHeight);
  } else if (scrollValue < 10) {
    $('.js-header').removeClass('header_stuck');
    $('.body').css('margin-top', 0);
  }
});

// TOGGLER
//
$('.js-toggler').click(function (event) {
  event.preventDefault();
});

//
$(document).ready(function () {
  //
  //// SM-MENU
  $('#js-navbar-pages-toggle').on('show.bs.collapse', function () {
    $('.header').addClass('header_sm-open');
    $('body').addClass('js-fixed');
    if ($('#js-navbar-phones-toggle').hasClass('show')) {
      $('.header__navbar-item_phones-toggler').removeClass('active');
      $('.header__navbar-icon_phone').removeClass('close-btn');
      $('#js-navbar-phones-toggle').removeClass('show');
    }
    $('.header__navbar-item_menu-toggler').addClass('active');
    $('.header__navbar-icon_menu').addClass('close-btn');
  });
  $('#js-navbar-pages-toggle').on('hide.bs.collapse', function () {
    $('.header').removeClass('header_sm-open');
    $('body').removeClass('js-fixed');
    $('.header__navbar-item_menu-toggler').removeClass('active');
    $('.header__navbar-icon_menu').removeClass('close-btn');
  });
  //
  //// PHONES-MENU_SM
  $('#js-navbar-phones-toggle').on('show.bs.collapse', function () {
    if ($('#js-navbar-pages-toggle').hasClass('show')) {
      $('#js-navbar-pages-toggle').removeClass('show');
    } else {
      $('.header').addClass('header_sm-open');
      $('body').addClass('js-fixed');
    }
    $('.header__navbar-item_phones-toggler').addClass('active');
    $('.header__navbar-icon_phone').addClass('close-btn');
    $('.header__navbar-item_menu-toggler').removeClass('active');
    $('.header__navbar-icon_menu').removeClass('close-btn');
  });
  $('#js-navbar-phones-toggle').on('hide.bs.collapse', function () {
    $('.header__navbar-item_phones-toggler').removeClass('active');
    $('.header__navbar-icon_phone').removeClass('close-btn');
    $('#js-navbar-pages-toggle').collapse('show');
  });
  //
  //// ORDER-CALL
  $('.header__navbar-link_order-call').click(function (event) {
    $('.header__header-form_order-call').addClass('show');
    $('.header__navbar-link_order-call').removeClass('show');
  });
  $('.header__input-btn_close').click(function (event) {
    $('.header__navbar-link_order-call').addClass('show');
    $('.header__header-form_order-call').removeClass('show');
  });
  //
  jQuery(function ($) {
    var options = {
      onComplete: function (num) {
        $('#js-order-call-send').addClass('show');
        $('#js-order-call-close').removeClass('show');
      },
      placeholder: "+7 (999) 999-99-99"
    };
    $("#header__order-call").mask("+9 (999) 999-99-99", options);
  });
  //
  //// SEARCH
  $('.js-search-toggler').click(function (event) {
    $('.header__navbar-item_search').addClass('show');
  });
  $('.header__input-btn_search-close').click(function (event) {
    $('.header__navbar-item_search').removeClass('show');
  });
  //
  //// USER LOGIN
  $('#js-user-login-toggle').on('show.bs.collapse', function () {
    $('.header__navbar-link_user').addClass('header__navbar-link_login');
    $('body').addClass('js-fixed');
  });
  $('#js-user-login-toggle').on('hide.bs.collapse', function () {
    $('.header__navbar-link_user').removeClass('header__navbar-link_login');
    $('body').removeClass('js-fixed');
  });

});
