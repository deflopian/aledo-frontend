var $productSlider = $('.project-aside__product-slider').flickity({
  prevNextButtons: false,
  pageDots: false,
  draggable: false,
  setGallerySize: false
});

$('.project-aside__product-links').on( 'mouseenter mouseleave', '.project-aside__product-link', function() {
  var selector = $(this).attr('data-selector');
  $productSlider.flickity( 'select', selector, false, true );
  //$carousel.flickity( 'selectCell', selector );
  console.log(selector);
});