var $carousel = $('.project-slider').flickity({
  // options  
  pageDots: true,
  wrapAround: false,
  contain: true,
  setGallerySize: false,
  draggable: getWindowSize()
});

function getWindowSize() {
  if ($(window).width() > 775) {
    return false
  }
  return true
}

var flkty = $carousel.data('flickity');
var flktyCells = flkty.cells.map(function(cell) {
  return cell.element.children["0"].src
});

$('.flickity-page-dots .dot').each(function(index) {
  $(this).css('background-image', 'url(' + flktyCells[index] + ')');
});
//
//
/* Fancybox */
$("[data-fancybox]").fancybox({
  loop: true,
  margin: 0,
  arrows: true,
  image: {
    preload : "auto"
  },
  buttons: [
    'close'
  ],
  clickContent: function( current, event ) {
    return current.type === 'image' ? 'close' : false;
  },
  clickOutside : 'close',
  thumbs: {
    autoStart : true,
    hideOnClose : true
  },
  lang: 'ru',
  i18n: {
    'en': {
      CLOSE: 'Close',
      NEXT: 'Next',
      PREV: 'Previous',
      ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
      PLAY_START: 'Start slideshow',
      PLAY_STOP: 'Pause slideshow',
      FULL_SCREEN: 'Full screen',
      THUMBS: 'Thumbnails'
    },
    'ru': {
      CLOSE: 'Закрыть',
      NEXT: 'Следующий',
      PREV: 'Предыдущий',
      ERROR: 'Ошибка: перезаргузите страницу.',
      PLAY_START: 'Старт',
      PLAY_STOP: 'Стоп',
      FULL_SCREEN: 'Полный экран',
      THUMBS: 'Меню'
    }
  }
});