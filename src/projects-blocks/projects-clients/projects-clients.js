/* inCLIENTS */

$('.projects-clients').flickity({
  // optilons  
  pageDots: false,
  prevNextButtons: false,
  wrapAround: true,
  freeScroll: true,
  dragThreshold: 10,
  freeScrollFriction: 0.05
});