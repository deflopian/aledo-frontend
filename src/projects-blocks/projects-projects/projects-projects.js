/* projects-projects */
function getHashFilter() {
  var hash = location.hash;
  // get filter=filterName
  var matches = location.hash.match( /filter=([^&]+)/i );
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent( hashFilter );
}

// init Isotope
var $grid = $('.projects-projects__grid').isotope({
  itemSelector: '.projects-projects__grid-item',
  layoutMode: 'packery',
  packery: {
    columnWidth: '.projects-projects__grid-sizer'
  },
  itemSelector: '.projects-projects__grid-item',
  percentPosition: true,
});
// bind filter button click
$('.projects-projects__filter').on( 'click', 'button', function() {
  var filterValue = $( this ).attr('data-filter');
  $grid.isotope({ filter: filterValue });
  // set filter in hash
  location.hash = 'filter=' + encodeURIComponent( filterValue ).replace(/^./,'');
});

// change is-checked class on buttons
var $filters = $('.projects-projects__button-group');

$filters.each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.active').removeClass('active');
    $( this ).addClass('active');
  });
});

var isIsotopeInit = false;

function onHashChange() {
  var hashFilter = getHashFilter();
  if ( !hashFilter || hashFilter == '' && isIsotopeInit ) {
    return;
  }
  isIsotopeInit = true;
  // filter isotope
  $grid.isotope({
    filter: '.' + hashFilter
  });
  // set selected class on button
  if ( hashFilter !== '' ) {
    $filters.find('.active').removeClass('active');
    $filters.find('[data-filter=".' + hashFilter + '"]').addClass('active');
  }  
};

$(function() {
  onHashChange();
});

$(window).on('hashchange', function() {
  onHashChange();
});

// filter name
$grid.on( 'arrangeComplete', function( event ) {
  var iso = $grid.data('isotope');
  if (iso.filteredItems.length < iso.items.length) {
    $('.projects-projects__button-group').addClass('filtered');
    var filterName = $('.projects-projects__button.active')[0].textContent;
    $('.projects-projects__filter-name').text( filterName );
    //
    $('.projects-clients').css({
      height: 0,
      opacity: 0
    });
    //
  } else {    
    $('.projects-projects__button-group').removeClass('filtered');
    //
    $('.projects-clients').css({
      height: 'inherit',
      opacity: 1
    });
    //
  }
});
  