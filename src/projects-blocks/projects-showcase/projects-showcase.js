/* projects-showcase */

var $carousel = $('.projects-showcase__slider').flickity({
  // options  
  pageDots: false,
  prevNextButtons: false,
  wrapAround: true,  
  setGallerySize: false,
  autoPlay: 4500,
  pauseAutoPlayOnHover: false,
  sync: '.projects-showcase__header',
  draggable: getWindowSize()
});

function getWindowSize() {
  if ($(window).width() > 775) {
    return false
  }
  return true
}

$('.projects-showcase__header').flickity({
  // options  
  pageDots: false,
  prevNextButtons: false,
  wrapAround: true,
  draggable: false,
  sync: '.projects-showcase__slider'
});

var showSelectedCell = function (data) {
  var thisSelected = data.selectedIndex + 1;
  var thisLenght = data.cells.length;
  $('.projects-showcase__cell-index').html( thisSelected );
  $('.projects-showcase__cells-lenght').html( thisLenght );
}

var flkty = $carousel.data('flickity');

showSelectedCell(flkty);

$carousel.on( 'select.flickity', function() {
  showSelectedCell(flkty);
});

$('.projects-showcase__arrow_previous').on( 'click', function() {
  $carousel.flickity( 'previous', true );
});

$('.projects-showcase__arrow_next').on( 'click', function() {
  $carousel.flickity( 'next', true );
});
