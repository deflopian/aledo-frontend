/* serie-aside */

/* LARGE TEXT LINE */
$(document).ready(function () {
  $('.thumbnails__name').mouseover(function () {
    var nameWidth = $(this).width();
    var text = $(this).children();
    var clone = text.clone(true, true)
      .css({
        position: 'absolute',
        visibility: 'hidden',
        textTransform: 'uppercase',
        fontWeight: 300,
        fontSize: '1.4rem',
        top: -1000,
        left: -1000
      })
      .appendTo("body");
    var textWidth = clone.width();
    clone.remove();
    if (nameWidth < textWidth) {
      var df = textWidth - nameWidth;
      var dur = df / 150;
      text.css({
        marginLeft: -df,
        transition: 'margin-left ' + dur + 's linear .2s'
      });
    }
  });
  $('.thumbnails__name').mouseout(function () {
    var text = $(this).children();
    text.css({
      marginLeft: 0
    });
  });
});

/* PANE SLIDER */

$('.serie-aside__slider').flickity({
  // options  
  wrapAround: false,
  contain: true,
  pageDots: false
});