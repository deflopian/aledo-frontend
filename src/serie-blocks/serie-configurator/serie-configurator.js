//
// POPOVERS
//
$('.aledo__popover-dismiss').popover({
  trigger: 'focus',
  html: true,
  //template: '<div class="aledo__popover popover" role="tooltip">' +
  //  '<div class="index-about-us__popover-title popover-title"></div>' +
  //  '<div class="aledo__popover-content popover-content"></div></div>'
});

var autoSizeText;

autoSizeText = function() {
  var el, elements, i, len, results;
  elements = $('.resize');
  if (elements.length < 0) {
    return;
  }
  results = [];
  for (i = 0, len = elements.length; i < len; i++) {
    el = elements[i];
    results.push((function(el) {
      var resizeText, results1;
      resizeText = function() {
        var elNewFontSize;
        elNewFontSize = (parseInt($(el).css('font-size').slice(0, -2)) - 1) + 'px';
        return $(el).css('font-size', elNewFontSize);
      };
      results1 = [];
      while (el.scrollHeight > el.offsetHeight) {
        results1.push(resizeText());
      }
      return results1;
    })(el));
  }
  return results;
};

$(document).ready(function() {
  return autoSizeText();
});

var $thumbnailsSlider = $('.serie-configurator__thumbnails-slider').flickity({
  prevNextButtons: false,
  pageDots: false,
  draggable: false,
  setGallerySize: false
});

$('.serie-configurator__btn-group_body-color').on( 'click', '.serie-configurator__btn_body-color', function() {
  var selector = $(this).attr('data-selector');
  $thumbnailsSlider.flickity( 'select', selector, false, true );
});