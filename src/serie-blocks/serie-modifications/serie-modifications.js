/* serie-modifications */

var $sortTable = $(".serie-modifications__tbody");

// init Isotope
var $table = $sortTable.isotope({
  itemSelector: ".serie-modifications__tr",
  layoutMode: "vertical",
  getSortData: {
    sort1: ".sort-1",
    sort2: ".sort-2",
    sort3: ".sort-3",
    sort4: ".sort-4",
    sort5: ".sort-5",
    sort6: ".sort-6",
    sort7: ".sort-7",
    sort8: ".sort-8",
    sort9: ".sort-9"
  }
});

updateFilterCounts();

// layout table after load page
$(window).on("load", function() {
  setTimeout(function() {
    $table.isotope("layout");
  }, 200);
});
// layout table after change tabs
$(window).on("hashchange", function() {
  $table.isotope("layout");
});

var isAscending = true;

// change is-checked class on buttons
$(".button-group").each(function(i, buttonGroup) {
  var $buttonGroup = $(buttonGroup);
  $buttonGroup.on("click", "button", function() {
    if($(this).hasClass("is-checked")){
      if(!isAscending) {
        $(this).removeClass("isDecending");
        $(this).addClass('isAscending');
      } else {
        $(this).removeClass("isAscending");
        $(this).addClass('isDecending');
      }
      isAscending = !isAscending;
    } else {
      $buttonGroup.find(".is-checked").removeClass("is-checked isAscending isDecending");
      $(this).addClass("is-checked isAscending");
      isAscending = true;
    }
  });
});

// bind sort button click
$("#sorts").on("click", "button", function() {
  var sortValue = $(this).attr("data-sort");
  $table.isotope({ sortBy: sortValue });
  $table.isotope({ sortAscending: isAscending });
});

// filter with selects and checkboxes
var $tableFilters = $('#serie-modification__filters_100359');
var $checkboxes = $("#serie-modification__filters_100359 input");
var $filtersReset = $tableFilters.find('.serie-modifications__filters-reset');

var filters = {};
function getComboFilter() {
  var combo = [];
  for ( var prop in filters ) {
    var group = filters[ prop ];
    if ( !group.length ) {
      // no filters in group, carry on
      continue;
    }
    // add first group
    if ( !combo.length ) {
      combo = group.slice(0);
      continue;
    }
    // add additional groups
    var nextCombo = [];
    // split group into combo: [ A, B ] & [ 1, 2 ] => [ A1, A2, B1, B2 ]
    for ( var i=0; i < combo.length; i++ ) {
      for ( var j=0; j < group.length; j++ ) {
        var item = combo[i] + group[j];
        nextCombo.push( item );
      }
    }
    combo = nextCombo;
  }
  var comboFilter = combo.join(', ');
  return comboFilter;
}

function updateFilterCounts()  {
  // get filtered item elements
  var itemElems = $table.isotope('getFilteredItemElements');
  var $itemElems = $( itemElems );
  var $checkboxes = $("#serie-modification__filters_100359 input");
  $checkboxes.each( function( i, elem ) {
    var filterValue = elem.value;
    if ( !filterValue ) {
      // do not update 'any' buttons
      return;
    }
    var count = $itemElems.filter( filterValue ).length;
    $elem = $( elem );
    var group;
    if (filters !== undefined) {
      for (var prop in filters) {
        if(filters[prop].length>0) {
          group = prop;
        }
      }
    }    
    var inGroup = $elem.parents('.serie-modifications__filters-col').attr('data-filter-group');    
    if(count == 0 && inGroup!==group) {
      elem.disabled = true;
      $elem.parent().addClass('isDisabled');
    } else {
      elem.disabled = false;
      $elem.parent().removeClass('isDisabled');
    }
  });
}

$tableFilters.on( 'change', function( event ) {
  var checkbox = event.target;
  var $checkbox = $( checkbox );
  var group = $checkbox.parents('.serie-modifications__filters-col').attr('data-filter-group');
  // create array for filter group, if not there yet
  var filterGroup = filters[ group ];
  if ( !filterGroup ) {
    filterGroup = filters[ group ] = [];
  }
  // add/remove filter
  if ( checkbox.checked ) {
    // add filter
    filterGroup.push( checkbox.value );
  } else {
    // remove filter
    var index = filterGroup.indexOf( checkbox.value );
    filterGroup.splice( index, 1 );
  }  
  var comboFilter = getComboFilter();
  $table.isotope({ filter: comboFilter });
  updateFilterCounts();
  var hashFilter; 
  if(!comboFilter) { hashFilter = '*'; } 
  else {
    hashFilter =  comboFilter.replace(/\./g, '.').replace(/\,\s/g, ',');    
  }
  // set filter in hash
  location.hash = 'filter=' + encodeURIComponent( hashFilter );
});

var isIsotopeInit = false;

function getHashFilter() {
  var hash = location.hash;
  // get filter=filterName
  var matches = location.hash.match( /filter=([^&]+)/i );
  var hashFilter = matches && matches[1].replace('.', '.').replace(',', ', ');
  return hashFilter && decodeURIComponent( hashFilter );
}

function onHashChange() {
  // hash for filter
  var hashFilter = getHashFilter();
  if ( !hashFilter || hashFilter == '' && isIsotopeInit ) {
    return;
  }
  // open tab
  $('.nav-link[title*="modification"]').tab("show");
  // filter isotope
  setTimeout(function() {
    isIsotopeInit = true;    
    $table.isotope({
      filter: hashFilter
    });    
  // get right hash back & reset
  var thatHash =  hashFilter.replace(/\./g, '.').replace(/\,\s/g, ',');
  location.hash = 'filter=' + encodeURIComponent( thatHash );
  }, 800);
  // set selected class on button
  if ( hashFilter !== '' ) {
    $checkboxes.each( function( i, elem ) {
      $elem = $( elem );
      if(hashFilter.indexOf(elem.value)>=0) elem.checked = true;
      updateFilterCounts();
    });
  }
}



// filters reset

function getFiltersReset() {
  var tableItemsCount = $table.isotope('getFilteredItemElements').length;
  //
  $table.on("arrangeComplete", function(event, laidOutItems) {
    var filteredCount = laidOutItems.length;
    if(filteredCount < tableItemsCount) { 
      $filtersReset.css({'visibility': 'visible', 'opacity': '1'});
    } else {
      $filtersReset.css('opacity','0');
      setTimeout(function() {
        $filtersReset.css('visibility', 'hidden');
      }, 600);
    }
  });
}

$(function() {
  onHashChange();
  getFiltersReset();
});

$filtersReset.on('click', function(e) {
  $table.isotope({ filter: '*' });
  $checkboxes.each( function( i, elem ) {
    elem.checked = false;
    $elem = $( elem );
    $elem.parent().removeClass('isDisabled');
  });
  location.hash = 'filter=' + encodeURIComponent( '*' );
  var props = Object.keys(filters);
  for (var i = 0; i < props.length; i++) {
    delete filters[props[i]];
  }  
});

var $cells = $(".serie-modifications__cell");
var $sorts = $("#sorts button");

// arrange sort buttons equal to cols
var colsHandler = function() {
  setTimeout(function() {
    $sorts.each(function(i) {
      var $cell = $(".cell-" + (i + 1))[0];
      var cellWidth = $cell.offsetWidth;
      this.style.width = cellWidth + "px";
    }); 
  }, 200);
};
$table.one("layoutComplete", function(event, laidOutItems) {
  colsHandler();
});
$(window).on("resize", function() {
    colsHandler();
});

// long text on sort buttons
$(document).ready(function() {
  $sorts.each(function(i) {
    var $sort = $($sorts[i]);
    $sort.mouseover(function() {
      var nameWidth = $(this).width();
      var text = $(this).children();
      var clone = text.clone(true, true);
      clone.css({
        position: "absolute",
        visibility: "hidden",
        fontSize: "1.2rem",
        fontWeight: "300",
        top: -1000,
        left: -1000
      });
      clone.appendTo("body");
      var textWidth = clone.width();
      clone.remove();
      if (nameWidth < textWidth) {
        var dur;
        var df = textWidth - nameWidth;
        if($(this).is('[class*="nding"]')) {
          dur = df * 0.015;
          text.css({
            marginLeft: -(df + 45),
            transition: "margin-left " + dur + "s linear"
          });
        } else {
          dur = df * 0.012;
          text.css({
            marginLeft: -(df + 5),
            transition: "margin-left " + dur + "s linear"
          });
        }
      }
    });
    $sort.mouseout(function() {
      var text = $(this).children();
      text.css({
        marginLeft: 0
      });
    });
  });
});

// more button 
var moreOver = $('.serie-modifications__moreover');
var modsTable = $('.serie-modifications__table');
var moreBtn = $('.serie-modifications__btn-more');

$table.on("layoutComplete", function(event, laidOutItems) {
  var tableHeight, moreOverHeight;
  tableHeight = modsTable.height();
  moreOverHeight = moreOver.height();
  
  setTimeout(function() {
    if(tableHeight<moreOverHeight) {
      moreOver.height(modsTable.height());
      moreBtn.fadeOut();
    } else {
      moreOver.height(427);
      moreBtn.fadeIn();
    }
  }, 400);
});

