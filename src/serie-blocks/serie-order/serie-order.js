/* serie-order */
var options = {
  placeholder: '1 шт.',
  reverse: true
};
$(".input-counter__input").mask("0## шт.", options);

var inputs = $(".input-counter__input").toArray();
var countUp = $(".input-counter__count_up").toArray();
var countDown = $(".input-counter__count_down").toArray();

inputValues = [];
inputs.forEach(function (input) {
  inputValues.push( parseInt(input.value.replace(' шт.', '')) );
});

countUp.forEach(function(button, index) {
  button.addEventListener('click', function() {    
    if (inputValues[index] > 0 && inputValues[index] < 1000) {      
      inputValues[index]++;
    }
    inputs[index].value = inputValues[index] + ' шт.';
  }); 
});

countDown.forEach(function(button, index) {
  button.addEventListener('click', function() {    
    if (inputValues[index] > 1 && inputValues[index] < 999) {      
      inputValues[index]--;
    }
    inputs[index].value = inputValues[index] + ' шт.';
  }); 
});

var orderMoreBtn = $('.serie-order__btn-more');