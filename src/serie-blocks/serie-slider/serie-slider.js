var $carousel = $('.serie-slider').flickity({
  // options  
  pageDots: false,
  wrapAround: true,
  cellAlign: 'left',
  dragThreshold: 10,
  setGallerySize: false
});