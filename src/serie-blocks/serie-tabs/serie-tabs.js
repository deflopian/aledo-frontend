/* serie-tabs */

var dropdown = $(".serie-tabs__nav-link_dropdown");
var dropdownMenu = $('.serie-downloads.show');
var navlink = $('.nav-link:not([class*="dropdown"])');
var panes = $(".serie-tabs__panes");
var special = $('.serie-description__special');

dropdown.on("show.bs.dropdown", function(e) {
  navlink.css("opacity", 0.75);
  panes.css("opacity", 0.75);
  location.hash = "tab=" + encodeURIComponent(e.target.title);
});

dropdown.on("hide.bs.dropdown", function() {
  navlink.css("opacity", 1);
  panes.css("opacity", 1);
});

function getHashTab() {
  var hash = location.hash;
  // get tab=TabName
  var matches = location.hash.match(/tab=([^&]+)/i);
  var hashTab = matches && matches[1];
  return hashTab && decodeURIComponent(hashTab);
}

navlink.on("shown.bs.tab", function(e) {  
  location.hash = "tab=" + encodeURIComponent(e.target.title);
});

var isTabInit = false;

$(function() {
  var hashTab = getHashTab();
  if (!hashTab && isTabInit) {
    return;
  }
  isTabInit = true;
  // TabhashTab Tab
  if (hashTab) {
    var thisNavlink = $('.nav-link[title*="' + hashTab + '"]');
    thisNavlink.tab("show");
  } 
  if (hashTab == dropdown[0].title) {
    $('.serie-tabs__nav-link_dropdown a[data-toggle*="dropdown"]').dropdown('toggle');
  }
  if (hashTab == special[0].title) {
    toSpecial();
  }
});

function toSpecial() {
  $('.nav-link[title*="description"]').tab("show");
  setTimeout(function() {
    window.scrollTo(0, special[0].offsetTop + special[0].offsetHeight);
  }, 200);
}