/* scCLIENTS */

$('.solutions-cinema-clients__slider').flickity({
  // optilons  
  pageDots: false,
  prevNextButtons: false,
  wrapAround: true,
  freeScroll: true,
  freeScrollFriction: 0.05
});

$(document).ready(function () {
  var $img = $('.solutions-cinema-clients__slide');
  var numItems = $img.length;
  for (var i = 0; i < numItems; i++) {
    that = $img[i];
    var bg = $(that).css('background-image');
    bg = bg.replace('url(', '').replace(')', '').replace(/\"/gi, "");
    img = new Image();
    img.src = bg;
    var imgWidth = img.width * .75;
    $(that).css({
      'width': imgWidth
    });
  }
  setTimeout(function () {
    $('.solutions-cinema-clients__slider').flickity('resize');
    $('.solutions-cinema-clients__slide').css('opacity', .7);
  }, 100);
});
