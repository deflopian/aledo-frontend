/* inPROJECTS */

$('.solutions-cinema-sample-modal__project-slider_dial').flickity({
  // options  
  pageDots: false
  ,wrapAround: true
  ,sync: '.solutions-cinema-sample-modal__project-slider_photos'
});

$('.solutions-cinema-sample-modal__project-slider_photos').flickity({
  // options  
  pageDots: false
  ,wrapAround: true
});

$('#js-solutions-cinema-sample-modal').on( 'shown.bs.modal', function( event ) {
  $('.solutions-cinema-sample-modal__project-slider_dial').flickity('resize');
  $('.solutions-cinema-sample-modal__project-slider_photos').flickity('resize');
});