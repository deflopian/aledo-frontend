/* SOLUTION-CINEMA */

// This function creates an <iframe> (and YouTube player)
// after the API code downloads.
var player;

function onYouTubeIframeAPIReady() {
  player = new YT.Player('solutions-cinema-video-banner__player', {
    height: '640',
    width: '360',
    videoId: 'PP0BXNqsfSE',
    playerVars: {
      'autoplay': 1,
      'controls': 0,
      'disablekb': 0,
      'modestbranding': 0,
      'loop': 1,
      'start': 6,
      'end': 9
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange,
    }
  });
}

// The API will call this function when the video player is ready.
function onPlayerReady(event) {
  event.target.setPlaybackQuality('small');
  event.target.mute();
  event.target.playVideo();
  if (event.target.getPlayerState() != 1) {
    $('.js-fader').css('opacity', '0');
  } else {
    $('.js-fader').css('opacity', '1');
  }
}
//
function onPlayerStateChange(event) {
  if (event.target.getPlayerState() == 0) {
    event.target.loadVideoById({
      'videoId': 'r0zai7-duS4',
      'startSeconds': 119,
      'endSeconds': 138
    });
  }
  if (event.target.getPlayerState() != 1) {
    $('.js-fader').css('opacity', '0');
  } else {
    $('.js-fader').css('opacity', '1');
  }
}

// SCROLL TO STICK
$(window).on('scroll', function (event) {
  var scrollValue = $(window).scrollTop();
  var windowsHeight = $(window).height();
  var scrollDif = windowsHeight - scrollValue;
  if (scrollDif < 235) {
    $('.solutions-cinema-header').addClass('solutions-cinema-header_stuck');
  } else
  if (scrollDif > 235) {
    $('.solutions-cinema-header').removeClass('solutions-cinema-header_stuck');
  }
});
//
$(document).ready(function () {
  var windowsHeight = $(window).height();
  $(document).on('click','#js-scroll-down',function(){
      $('html, body ').animate({
        scrollTop: $(".solutions-cinema-video-banner").height()
      }, 600);
  });
});
