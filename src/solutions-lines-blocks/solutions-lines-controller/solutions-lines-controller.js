/* SOLUTIONS LINES CONTROLLER */

$(window).on("load", function () {
  if ($(window).width() > 768) {
    setTimeout(function () {
      $('.solutions-lines-controller__welcome').fadeOut('slow')
    }, 3500);
    setTimeout(function () {
      $('.solutions-lines-controller__welcome-instructions').fadeIn('slow')
    }, 500);
    setTimeout(function () {
      $('.solutions-lines-controller__welcome-instructions').fadeOut('slow')
    }, 10500);
  }
});
//
$(window).on("click", function () {
  $('.solutions-lines-controller__welcome').fadeOut('slow');
  $('.solutions-lines-controller__welcome-instructions').fadeOut('slow');
});

// SCROLL DOWN
//
$(document).ready(function () {
  var windowsHeight = $(window).height();
  $(document).on('click','#js-scroll-down',function(){
      $('html, body ').animate({
        scrollTop: $(".solutions-lines-controller").height()
      }, 600);
  });
});

//
$(".solutions-lines-controller__slider_dimming ").on("slidechange", function (event, ui) {
  var pos = $('.solutions-lines-controller__slider-handle_dimming').text();
  var num = parseInt(pos);
  $('.solutions-lines-controller__slider-back .dim-c').css('transform', 'scale(.' + num + ')');
  console.log('changed ' + num);
});
