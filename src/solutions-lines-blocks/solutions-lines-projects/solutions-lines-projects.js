/* inPROJECTS */

// ELLIPSIS
function csProjectShort() {
  $(function () {
    if ($(window).width() > 991) {
      $('.solutions-lines-projects__project-desc').succinct({
        size: 360
      });
    } else {
      $('.solutions-lines-projects__project-desc').succinct({
        size: 240
      });
    }
  });
}
$(document).ready(csProjectShort);
$(window).resize(csProjectShort);

//
// NAV SLIDER
var $carousel = $('.solutions-lines-projects__projects-nav').flickity({
  // options  
  pageDots: false,
  wrapAround: true,
  contain: true,
  setGallerySize: false,
  sync: '.solutions-lines-projects__projects-wrap'
});

$carousel.on('staticClick.flickity', function (event, pointer, cellElement, cellIndex) {
  $carousel.flickity('select', cellIndex);
});

// PROJECT SLIDER
$('.solutions-lines-projects__projects-wrap').flickity({
  // options  
  pageDots: false,
  prevNextButtons: false,
  wrapAround: true,
  draggable: false,
  setGallerySize: false 
});

// DETAILS SLIDER
$('.solutions-lines-projects__project-slider').flickity({
  // options  
  pageDots: false,
  wrapAround: true,
  setGallerySize: false
});

/* LARGE TEXT LINE */

$(document).ready(function () {
  $('.thumbnails__name').mouseover(function () {
    var nameWidth = $(this).width();
    var text = $(this).children();
    var clone = text.clone(true, true);
    clone.css({
      position: 'absolute',
      visibility: 'hidden',
      textTransform: 'uppercase',
      fontSize: '1.4rem',
      top: -1000,
      left: -1000
    })
    if ( $(this).parent().hasClass( "is-selected" ) ) {
      clone.css({
        fontWeight: 500
      })
      console.log(clone)
    } else {
      clone.css({
        fontWeight: 300
      })
    }      
    clone.appendTo("body");
    var textWidth = clone.width();
    clone.remove();
    if (nameWidth < textWidth) {
      var df = textWidth - nameWidth;
      var dur = df / 150;
      text.css({
        marginLeft: -df,
        transition: 'margin-left ' + dur + 's linear .2s'
      });
    }
  });
  $('.thumbnails__name').mouseout(function () {
    var text = $(this).children();
    text.css({
      marginLeft: 0
    });
  });
});
